@extends('layout.default')
@section('breadcrumb')
<div class="page-header">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Rekap Suara</span> - per
                Daerah</h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        <div class="header-elements d-none py-0 mb-3 mb-md-0">
            <div class="breadcrumb">
                <a href="index.html" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <span class="breadcrumb-item active">Rekap</span>
            </div>
        </div>
    </div>
</div>
@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h6 class="card-title">
                        <b>
                            GRAFIK REALTIME PEMILU {{ $title }}
                        </b>
                    </h6>
                </div>

                <div class="card-body py-0">
                    <div class="row">
                        <div class="col-md-12">
                            <form action="{{ route('rekap') }}" method="POST">
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <select class="form-control select-search" id="kecamatan" name="kecamatan"
                                                data-fouc>
                                                <option selected disabled><b>Pilih Kecamatan</b></option>
                                                @foreach ($data['kecamatan'] as $item)
                                                <option value="{{ $item['id'] }}">{{ strtoupper($item['nama']) }}
                                                </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <select class="form-control select-search" name="kelurahan" data-fouc
                                                id="kel">
                                                <option selected disabled><b>Pilih Kelurahan</b></option>
                                                {{-- <option selected disabled> Pilih Kelurahan</option> --}}
                                                {{-- </optgroup> --}}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2 text-center">
                                        <input type="submit" class="w-50 btn btn-primary" value="Cari" />
                                    </div>
                                </div>
                                @method('POST')
                                @csrf
                            </form>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="chart-container">
                                <div class="chart has-fixed-height" id="bars_stacked"></div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>
<script>
    $("#kecamatan").change(function () {
        // var vals = ['Please choose from above'];
        // var $secondChoice = $("#kel");
        // $secondChoice.empty();
        // $.each(vals, function(index, value) {
        //     $secondChoice.append("<option>" + value + "</option>");
        // });
        var hashid = $(this).val();
        // // console.log("1:" + id);
        // console.log("2:" + dataString);
        // var url = "/get/kelurahan/"+id;
        var select = document.getElementById('kel')
        select.innerHTML = "<option selected disabled><b>Pilih Kelurahan</b></option>";
        // select.empty();
        $.ajax({
            type: "GET",
            url: "{{ url('/get/kelurahan') }}",
            data: {
                id: hashid
            },
            cache: false,
            success: function (data) {
                // console.log(data['status']);
                var options = "";
                data = data['data'];
                data.forEach(function (item) {
                    select.append(new Option(item.nama.toUpperCase(), item.id));
                });
            },
        });

    });
</script>
@isset($data['rekap'])
@if (count($data['rekap']['Rekap']) < 1)
<script>
    $(document).ready(function() {
        (  new PNotify({
            title: 'Peringatan',
            text: 'Data belum terisi',
            // addclass: 'bg-primary border-primary',
            type: 'info',
            addclass: 'bg-warning border-warning',
            hide: false
        }));
    });
</script>
@endif
<script>
    var EchartsBarsStackedLight = function () {
        // Stacked bar chart
        var _barsStackedLightExample = function () {
            if (typeof echarts == 'undefined') {
                console.warn('Warning - echarts.min.js is not loaded.');
                return;
            }

            // Define element
            var bars_stacked_element = document.getElementById('bars_stacked');

            //
            // Charts configuration
            //

            if (bars_stacked_element) {

                // Initialize chart
                var bars_stacked = echarts.init(bars_stacked_element);


                //
                // Chart config
                //

                // Options
                bars_stacked.setOption({

                    // Global text styles
                    textStyle: {
                        fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                        fontSize: 13
                    },

                    // Chart animation duration
                    animationDuration: 750,

                    // Setup grid
                    grid: {
                        left: 0,
                        right: 30,
                        top: 35,
                        bottom: 0,
                        containLabel: true
                    },

                    // Add legend
                    legend: {
                        data: ['Ditho & Dewi', 'Kotak Kosong'],
                        itemHeight: 8,
                        itemGap: 20,
                        textStyle: {
                            padding: [0, 0]
                        }
                    },

                    // Add tooltip
                    tooltip: {
                        trigger: 'axis',
                        backgroundColor: 'rgba(0,0,0,0.75)',
                        padding: [10, 15],
                        textStyle: {
                            fontSize: 13,
                            fontFamily: 'Roboto, sans-serif'
                        },
                        axisPointer: {
                            type: 'shadow',
                            shadowStyle: {
                                color: 'rgba(0,0,0,0.025)'
                            }
                        }
                    },

                    // Horizontal axis
                    xAxis: [{
                        type: 'value',
                        axisLabel: {
                            color: '#333'
                        },
                        axisLine: {
                            lineStyle: {
                                color: '#999'
                            }
                        },
                        splitLine: {
                            show: true,
                            lineStyle: {
                                color: '#eee',
                                type: 'dashed'
                            }
                        }
                    }],

                    // Vertical axis
                    yAxis: [{
                        type: 'category',
                        data: @php echo $data['name']; @endphp,
                        axisLabel: {
                            color: '#333'
                        },
                        axisLine: {
                            lineStyle: {
                                color: '#999'
                            }
                        },
                        splitLine: {
                            show: true,
                            lineStyle: {
                                color: ['#eee']
                            }
                        },
                        splitArea: {
                            show: true,
                            areaStyle: {
                                color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.015)']
                            }
                        }
                    }],

                    // Add series
                    series: [
                        {
                            name: 'Ditho & Dewi',
                            type: 'bar',
                            stack: 'Total',
                            barWidth: 36,
                            itemStyle: {
                                normal: {
                                    color: '#F5C324',
                                    label: {
                                        show: true,
                                        position: 'insideRight',
                                        padding: [0, 10],
                                        fontSize: 12
                                    }
                                }
                            },
                            data:@php echo $data['hasil_suara_satu']; @endphp,
                        },
                        {
                            name:  'Kotak Kosong',
                            type: 'bar',
                            stack: 'Total',
                            barWidth: 36,
                            itemStyle: {
                                normal: {
                                    color: '#FF8A65',
                                    label: {
                                        show: true,
                                        position: 'insideRight',
                                        padding: [0, 10],
                                        fontSize: 12
                                    }
                                }
                            },
                            data:@php echo $data['hasil_suara_kosong']; @endphp,
                        }


                    ]
                });
            }


            //
            // Resize charts
            //

            // Resize function
            var triggerChartResize = function () {
                bars_stacked_element && bars_stacked.resize();
            };

            // On sidebar width change
            var sidebarToggle = document.querySelector('.sidebar-control');
            sidebarToggle && sidebarToggle.addEventListener('click', triggerChartResize);

            // On window resize
            var resizeCharts;
            window.addEventListener('resize', function () {
                clearTimeout(resizeCharts);
                resizeCharts = setTimeout(function () {
                    triggerChartResize();
                }, 200);
            });
        };


        //
        // Return objects assigned to module
        //

        return {
            init: function () {
                _barsStackedLightExample();
            }
        }
    }();


    document.addEventListener('DOMContentLoaded', function () {
        EchartsBarsStackedLight.init();
    });

</script>
@endisset

@endsection
