@extends('layout.default')
@section('breadcrumb')
<div class="page-header">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Rekap Suara</span> - per
                Daerah</h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        <div class="header-elements d-none py-0 mb-3 mb-md-0">
            <div class="breadcrumb">
                <a href="index.html" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <span class="breadcrumb-item active">Rekap</span>
            </div>
        </div>
    </div>
</div>
@endsection
@section('content')
<div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h6 class="card-title">
                        <b>
                            REKAP FORM C1
                        </b>
                    </h6>
                </div>
                <div class="card-body py-0">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <form action="{{ route('act.rekap.form') }}" method="POST">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <select class="form-control select-search" id="kecamatan" name="kecamatan"
                                                        data-fouc>
                                                        <option selected disabled><b>Pilih Kecamatan</b></option>
                                                        @foreach ($data['kecamatan'] as $item)
                                                        <option value="{{ $item['id'] }}">{{ strtoupper($item['nama']) }}
                                                        </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <select class="form-control select-search" name="kelurahan" data-fouc
                                                        id="kel">
                                                        <option selected disabled><b>Pilih Kelurahan</b></option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-2 text-center">
                                                <input type="submit" class="w-50 btn btn-primary" value="Cari" />
                                            </div>
                                        </div>
                                        @method('POST')
                                        @csrf
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table  datatable-basic">

                                <thead>
                                    <tr>
                                        <th>NO</th>
                                        <th>TPS</th>
                                        <th>Suara Dhito & Dewi</th>
                                        <th>Suara Kosong</th>
                                        <th>Suara Tidak Sah</th>
                                        <th>Suara Masuk</th>
                                        <th>Form C1</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @isset($data['formc1'])
                                        @foreach ($data['formc1'] as $index => $item)
                                        <tr>
                                            <td>{{ ($index+1) }}</td>
                                            <td>{{ $item['name'] }}</td>
                                            <td>{{ $item['hasil_suara_satu'] }}</td>
                                            <td>{{ $item['hasil_suara_kosong'] }}</td>
                                            <td>{{ $item['total_suara'] }}</td>
                                            <td>{{ $item['suara_tidak_sah'] }}</td>
                                            <td>
                                                <a target="#" href="http://assets.bravodhitodewi.com/c1/{{ $item['link'] }}">Lihat Form C1</a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    @endisset

                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>

            </div>
        </div>
</div>
<script>
    $("#kecamatan").change(function () {
        // var vals = ['Please choose from above'];
        // var $secondChoice = $("#kel");
        // $secondChoice.empty();
        // $.each(vals, function(index, value) {
        //     $secondChoice.append("<option>" + value + "</option>");
        // });
        var hashid = $(this).val();
        // // console.log("1:" + id);
        // console.log("2:" + dataString);
        // var url = "/get/kelurahan/"+id;
        var select = document.getElementById('kel')
        select.innerHTML = "<option selected disabled><b>Pilih Kelurahan</b></option>";
        // select.empty();
        $.ajax({
            type: "GET",
            url: "{{ url('/get/kelurahan') }}",
            data: {
                id: hashid
            },
            cache: false,
            success: function (data) {
                // console.log(data['status']);
                var options = "";
                data = data['data'];
                data.forEach(function (item) {
                    select.append(new Option(item.nama.toUpperCase(), item.id));
                });
            },
        });

    });
</script>
@endsection
