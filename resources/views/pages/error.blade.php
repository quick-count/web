<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Limitless - Responsive Web Application Kit by Eugene Kopyov</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="{{ asset('global_assets/css/icons/icomoon/styles.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('layout_assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('layout_assets/css/bootstrap_limitless.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('layout_assets/css/layout.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('layout_assets/css/components.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('layout_assets/css/colors.min.css') }}" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script src="{{ asset('global_assets/js/main/jquery.min.js') }}"></script>
	<script src="{{ asset('global_assets/js/main/bootstrap.bundle.min.js') }}"></script>
	<script src="{{ asset('global_assets/js/plugins/loaders/blockui.min.js') }}"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script src="{{ asset('global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>

	<script src="{{ asset('layout_assets/js/app.js') }}"></script>
    <script src="{{ asset('global_assets/js/demo_pages/login.js') }}"></script>


	<!-- Theme JS files -->
	<script src="{{ asset('global_assets/js/plugins/notifications/pnotify.min.js') }}"></script>

	<script src="{{ asset('layout_assets/js/app.js') }}"></script>
    <script src="{{ asset('global_assets/js/demo_pages/extra_pnotify.js') }}"></script>

	<!-- /theme JS files -->

</head>
<body >
	<!-- Page content -->
	<div class="page-content">

		<!-- Main content -->
		<div class="content-wrapper">
			<!-- Content area -->
			<div class="content d-flex justify-content-center align-items-center">
				<!-- Login card -->
                <form class="login-form" action="{{ route('login') }}" method="POST">
                    @csrf
					<div class="card mb-0">
						<div class="card-body">
							<div class="text-center mb-3">
								<i class="icon-people icon-2x text-warning-400 border-warning-400 border-3 rounded-round p-3 mb-3 mt-1"></i>
								<h5 class="mb-0">Login to your account</h5>
								<span class="d-block text-muted">Your credentials</span>
							</div>

							<div class="form-group form-group-feedback form-group-feedback-left">
								<input type="text" class="form-control" name="username" placeholder="Username">
								<div class="form-control-feedback">
									<i class="icon-user text-muted"></i>
								</div>
							</div>

							<div class="form-group form-group-feedback form-group-feedback-left">
								<input type="password" class="form-control" name="password" placeholder="Password">
								<div class="form-control-feedback">
									<i class="icon-lock2 text-muted"></i>
								</div>
							</div>


							<div class="form-group">
								<button type="submit" class="btn btn-primary btn-block">Sign in <i class="icon-circle-right2 ml-2"></i></button>
							</div>

                            {{-- <div class="form-group d-flex align-items-center">

								<a href="login_password_recover.html" class="ml-auto">Forgot password?</a>
							</div> --}}





						</div>
					</div>
				</form>
				<!-- /login card -->
			</div>
			<!-- /content area -->

		</div>
		<!-- /main content -->

    </div>

    @if ($message = Session::get('gagal'))
        <script>
            $(document).ready(function() {
                (  new PNotify({
                    title: 'Terjadi kesalahan',
                    text: '{{ $message }}',
                    addclass: 'bg-primary border-primary',
                    type: 'info',
                    addclass: 'bg-danger border-danger',
                    hide: false
                }));
            });
        </script>
    @endif

</body>
</html>
