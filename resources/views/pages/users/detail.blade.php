@extends('layout.default')
@section('breadcrumb')
<div class="page-header">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">User</span> - Cari user</h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        <div class="header-elements d-none py-0 mb-3 mb-md-0">
            <div class="breadcrumb">
                <a href="index.html" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> User</a>
                <span class="breadcrumb-item active">Cari</span>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">Data User</legend>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Nama Saksi</label>
                        <div class="col-lg-10">
                        <input type="text" class="form-control" readonly value="{{ ucfirst($data['saksi']['nama']) }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Username</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" readonly value="{{ ucfirst($data['username']) }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Nama TPS</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" readonly value="{{ ucfirst($data['tps']['nama']) }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Kecamatan</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" readonly value="{{ ucfirst($data['Daerah']['kecamatan']) }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Kelurahan</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" readonly value="{{ ucfirst($data['Daerah']['kelurahan']) }}">
                        </div>
                    </div>
                </fieldset>
            <form action="{{ route('act.user.form') }}" method="POST">
                @csrf
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">Form C1</legend>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Dhito & Dewi</label>
                        <div class="col-lg-10">
                        <input type="number" class="form-control" name="suarasatu"  value="{{ $c1['hasil_suara_satu'] }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Kotak Kosong</label>
                        <div class="col-lg-10">
                            <input type="number" class="form-control"    name="suarakosong"  value="{{ $c1['hasil_suara_kosong'] }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Tidak Sah</label>
                        <div class="col-lg-10">
                            <input type="number" class="form-control"   name="tidaksah"  value="{{ $c1['suara_tidak_sah'] }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Total Suara</label>
                        <div class="col-lg-10">
                            <input type="number" class="form-control"  name="totalsuara" value="{{ $c1['total_suara'] }}">
                        </div>
                    </div>
                <input type="hidden" name="userid" value="{{ ucfirst($data['id']) }}">
                <input type="hidden" name="tpsid" value="{{ ucfirst($data['tps']['id']) }}">
                </fieldset>
                <div class="text-right">
                    <button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
                </div>
            </form>
        </div>
    </div>
@endsection




