@extends('layout.default')
@section('breadcrumb')
<div class="page-header">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">User</span> - Cari user</h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        <div class="header-elements d-none py-0 mb-3 mb-md-0">
            <div class="breadcrumb">
                <a href="index.html" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> User</a>
                <span class="breadcrumb-item active">Cari</span>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h6 class="card-title">
                        <b>
                            REKAP FORM C1
                        </b>
                    </h6>
                </div>
                <div class="card-body py-0">
                    <form action="{{ route('act.user.cari') }}" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <select class="form-control select-search" name="jenis" data-fouc id="kel">
                                        <option value="Username">Username</option>
                                        <option value="Kelurahan">Kelurahan</option>
                                        <option value="Kecamatan">Kecamatan</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="cari"
                                        placeholder="Ketikan sesuatu disini..... " name="text" id="">
                                </div>
                            </div>
                            <div class="col-md-1 ">
                                <input type="submit" class=" col-md-12 btn btn-primary" value="Cari">
                            </div>
                        </div>
                    </form>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table  datatable-basic">

                                <thead>
                                    <tr>
                                        <th width="5px">NO</th>
                                        <th>Username</th>
                                        <th>Kecamatan, Kelurahn</th>
                                        <th>TPS</th>
                                        <th width="5px">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @isset($data['users'])
                                        @foreach ($data['users'] as $index => $item)
                                        <tr>
                                            <td>{{ ($index+1) }}</td>
                                            <td>{{ Ucfirst($item['username']) }}</td>
                                            <td>{{ Ucfirst($item['kec']) }}, {{ Ucfirst($item['kel']) }}</td>
                                            <td>{{ Ucfirst($item['nama_tps']) }}</td>
                                            <td>
                                                <form action="{{ route('act.user.detail') }}" method="post">
                                                <input type="hidden" name="id" value="{{ $item['id'] }}">
                                                    <input type="Submit" value="Lihat" class="btn btn-primary">
                                                    @csrf
                                                </form>
                                            </td>

                                        </tr>
                                        @endforeach
                                    @endisset

                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>

            </div>
        </div>
</div>
<script>
    $("#kecamatan").change(function () {
        // var vals = ['Please choose from above'];
        // var $secondChoice = $("#kel");
        // $secondChoice.empty();
        // $.each(vals, function(index, value) {
        //     $secondChoice.append("<option>" + value + "</option>");
        // });
        var hashid = $(this).val();
        // // console.log("1:" + id);
        // console.log("2:" + dataString);
        // var url = "/get/kelurahan/"+id;
        var select = document.getElementById('kel')
        select.innerHTML = "<option selected disabled><b>Pilih Kelurahan</b></option>";
        // select.empty();
        $.ajax({
            type: "GET",
            url: "{{ url('/get/kelurahan') }}",
            data: {
                id: hashid
            },
            cache: false,
            success: function (data) {
                // console.log(data['status']);
                var options = "";
                data = data['data'];
                data.forEach(function (item) {
                    select.append(new Option(item.nama.toUpperCase(), item.id));
                });
            },
        });

    });
</script>
@endsection




