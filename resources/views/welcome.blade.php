<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>QuickCount Kediri</title>

    <!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="{{ asset('global_assets/css/icons/icomoon/styles.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('layout_assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('layout_assets/css/bootstrap_limitless.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('layout_assets/css/layout.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('layout_assets/css/components.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('layout_assets/css/colors.min.css') }}" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script src="{{ asset('global_assets/js/main/jquery.min.js') }}"></script>
	<script src="{{ asset('global_assets/js/main/bootstrap.bundle.min.js') }}"></script>
	<script src="{{ asset('global_assets/js/plugins/loaders/blockui.min.js') }}"></script>
	<script src="{{ asset('global_assets/js/plugins/ui/slinky.min.js') }}"></script>
	<script src="{{ asset('global_assets/js/plugins/ui/ripple.min.js') }}"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	{{-- <script src="{{ asset('global_assets/js/plugins/visualization/d3/d3.min.js') }}"></script> --}}
	{{-- <script src="{{ asset('global_assets/js/plugins/visualization/d3/d3_tooltip.js') }}"></script> --}}
	{{-- <script src="{{ asset('global_assets/js/plugins/forms/styling/switchery.min.js') }}"></script> --}}
	{{-- <script src="{{ asset('global_assets/js/plugins/ui/moment/moment.min.js') }}"></script> --}}
	{{-- <script src="{{ asset('global_assets/js/plugins/pickers/daterangepicker.js') }}"></script> --}}

    <script src="{{ asset('layout_assets/js/app.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/visualization/echarts/echarts.min.js') }}"></script>
	<script src="{{ asset('global_assets/js/demo_pages/dashboard.js') }}"></script>
	{{-- <script src="{{ asset('global_assets/js/demo_charts/pages/dashboard/light/donuts.js') }}"></script> --}}
	{{-- <script src="{{ asset('global_assets/js/demo_charts/pages/dashboard/light/bars.js') }}"></script> --}}
	{{-- <script src="{{ asset('global_assets/js/demo_charts/pages/dashboard/light/pies.js') }}"></script> --}}
    <!-- /theme JS files -->

	<script src="{{ asset('global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
	<script src="{{ asset('global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/demo_pages/form_select2.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>

	<script src="{{ asset('global_assets/js/plugins/notifications/pnotify.min.js') }}"></script>

	<script src="{{ asset('layout_assets/js/app.js') }}"></script>
	<!-- Theme JS files -->
	<script src="{{ asset('global_assets/js/demo_pages/datatables_basic.js') }}"></script>

	{{-- <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/responsive.min.js') }}"></script> --}}

	{{-- <script src="{{ asset('global_assets/js/demo_pages/datatables_responsive.js') }}"></script> --}}


</head>
	<!-- /theme JS files -->
<body>
    @include('layout.sidebar')
    <div class="page-content pt-0">

		<!-- Main content -->
		<div class="content-wrapper">

			<!-- Content area -->
			<div class="content">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header header-elements-inline">
                                <h6 class="card-title"><b>Grafik Realtime Pemilu</b></h6>
                                <div class="header-elements">
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="card p-0">
                                            <div class="card-img-top text-center pb-2" style="height: 200px !important;">
                                                <img src="{{ asset('img/main.jpg') }}" style="max-height: 100%;" alt="" srcset="">
                                            </div>
                                            {{-- <img  src="..." alt="Card image cap"> --}}
                                            <div class="card-body py-2 pr-0 text-white" style="background: #F5C324 !important">
                                                <h1 class="" style="font-size: 3.5em;">
                                                    <b>{{ number_format($data['hasil_suara_satu']) }}</b></h1>
                                                <h6 class="card-subtitle mb-2">DHITO & DEWI</h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="card p-0">
                                            <div class="card-img-top text-center" style="height: 200px !important;">
                                                {{-- <h1>KOSONG</h1> --}}
                                            </div>
                                            <div class="card-body py-2 pr-0  text-white" style="background: #FF8A65 !important">
                                                <h1 class="" style="font-size: 3.5em;">
                                                    <b>{{ number_format($data['hasil_suara_kosong']) }}</b></h1>
                                                <h6 class="card-subtitle mb-2">KOTAK KOSONG</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">

                                    <div class="col-lg-6">
                                        <div class="card p-0  text-white" style="background: #39C86A;">
                                            <div class="card-body py-2 pr-0">
                                                <h1 class="" style="font-size: 4em;">
                                                    <b>{{ number_format($data['total_suara']) }}</b></h1>
                                                <h6 class="card-subtitle mb-2">Total Suara Masuk</h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="card p-0 text-white" style="background: #FF5722;">
                                            <div class="card-body py-2 pr-0">
                                                <h1 class="" style="font-size: 4em;">
                                                    <b>{{ number_format($data['suara_tidak_sah']) }}</b></h1>
                                                <h6 class="card-subtitle mb-2">Total Suara Tidak Sah</h6>
                                            </div>
                                        </div>
                                    </div>

                                </div>


                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-body  pt-5 pb-5 ">
                                <div class="row ">
                                    <div class="col-lg-12 ">
                                        <div class="chart-container pb-3 ">
                                            <div class="chart has-fixed-height" id="pie_basic"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 text-center">
                                        <h1 class="" >
                                            <b> Perolehan Suara Dari {{ number_format($total) }} TPS</b></h1>
                                        {{-- <div class="card "> --}}
                                            {{-- <div class="card-body text-white" style="background: #FF8A65 !important"> --}}

                                            {{-- </div> --}}
                                        {{-- </div> --}}
                                    </div>
                                </div>

                                {{-- <div class="row">
                                    <div class="col-md-12">
                                        {{ $total }}
                                    </div>
                                </div> --}}
                            </div>

                        </div>
                    </div>
                </div>




                <script type="text/javascript">
                    setTimeout(function(){
                        location.reload();
                    },30000);
                 </script>
                <script>
                    var EchartsPieBasicLight = function () {
                        var _scatterPieBasicLightExample = function () {
                            if (typeof echarts == 'undefined') {
                                console.warn('Warning - echarts.min.js is not loaded.');
                                return;
                            }

                            // Define element
                            var pie_basic_element = document.getElementById('pie_basic');


                            //
                            // Charts configuration
                            //

                            if (pie_basic_element) {

                                // Initialize chart
                                var pie_basic = echarts.init(pie_basic_element);


                                //
                                // Chart config
                                //

                                // Options
                                pie_basic.setOption({

                                    // Colors
                                    color: [
                                        // '#2ec7c9','#b6a2de','#5ab1ef','#ffb980','#d87a80',
                                        // '#8d98b3','#e5cf0d','#97b552','#95706d','#dc69aa',
                                        '#588dd5', '#f5994e', '#c05050',
                                        // '#59678c','#c9ab00','#7eb00a','#6f5553','#c14089'
                                    ],

                                    // Global text styles
                                    textStyle: {
                                        fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                                        fontSize: 13
                                    },

                                    // Add title
                                    title: {
                                        text: 'Total Perhitungan Suara PILKADA',
                                        subtext: 'Dhito dan Dewi',
                                        left: 'center',
                                        textStyle: {
                                            fontSize: 17,
                                            fontWeight: 500
                                        },
                                        subtextStyle: {
                                            fontSize: 12
                                        }
                                    },

                                    // Add tooltip
                                    tooltip: {
                                        trigger: 'item',
                                        backgroundColor: 'rgba(0,0,0,0.75)',
                                        padding: [10, 15],
                                        textStyle: {
                                            fontSize: 13,
                                            fontFamily: 'Roboto, sans-serif'
                                        },
                                        formatter: "{a} <br/>{b}: {c} ({d}%)"
                                    },

                                    // Add legend
                                    legend: {
                                        orient: 'vertical',
                                        top: 'center',
                                        left: 0,
                                        data: ['Dhito & Dewi', 'Kotak Kosong'],
                                        itemHeight: 8,
                                        itemWidth: 8
                                    },

                                    // Add series
                                    series: [{
                                        name: 'Browsers',
                                        type: 'pie',
                                        radius: '70%',
                                        center: ['50%', '57.5%'],
                                        itemStyle: {
                                            normal: {
                                                borderWidth: 1,
                                                borderColor: '#fff'
                                            }
                                        },
                                        data: [{
                                                value: @php echo $data['hasil_suara_satu'];@endphp,
                                                name: 'Dhito & Dewi'
                                            },
                                            {
                                                value: @php echo $data['hasil_suara_kosong'];@endphp,
                                                name: 'Kotak Kosong'
                                            },
                                        ]
                                    }]
                                });
                            }


                            //
                            // Resize charts
                            //

                            // Resize function
                            var triggerChartResize = function () {
                                pie_basic_element && pie_basic.resize();
                            };

                            // On sidebar width change
                            var sidebarToggle = document.querySelector('.sidebar-control');
                            sidebarToggle && sidebarToggle.addEventListener('click', triggerChartResize);

                            // On window resize
                            var resizeCharts;
                            window.addEventListener('resize', function () {
                                clearTimeout(resizeCharts);
                                resizeCharts = setTimeout(function () {
                                    triggerChartResize();
                                }, 200);
                            });
                        };

                        return {
                            init: function () {
                                _scatterPieBasicLightExample();
                            }
                        }
                    }();

                    document.addEventListener('DOMContentLoaded', function () {
                        EchartsPieBasicLight.init();
                    });

                </script>
			</div>
			<!-- /content area -->

		</div>
		<!-- /main content -->

	</div>

    {{-- @include('layout.header')

	<!-- Page header -->
    @yield('breadcrumb')
	<!-- /page header -->
	<!-- Page content -->
	<div class="page-content pt-0">

		<!-- Main content -->
		<div class="content-wrapper">

			<!-- Content area -->
			<div class="content">
                @yield('content')

			</div>
			<!-- /content area -->

		</div>
		<!-- /main content -->

	</div>
	<!-- /page content -->

    @include('layout.footer') --}}

    @isset($gagal)
        <script>
            $(document).ready(function() {
                (  new PNotify({
                    title: 'Terjadi kesalahan',
                    text: '{{ $gagal }}',
                    addclass: 'bg-primary border-primary',
                    type: 'info',
                    addclass: 'bg-danger border-danger',
                    hide: false
                }));
            });
        </script>
    @endisset
    @if ($message = Session::get('gagal'))
        <script>
            $(document).ready(function() {
                (  new PNotify({
                    title: 'Terjadi kesalahan',
                    text: '{{ $message }}',
                    addclass: 'bg-primary border-primary',
                    type: 'info',
                    addclass: 'bg-danger border-danger',
                    hide: false
                }));
            });
        </script>
    @endif




</body>
</html>
