<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>QuickCount Kediri</title>

    <!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="{{ asset('global_assets/css/icons/icomoon/styles.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('layout_assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('layout_assets/css/bootstrap_limitless.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('layout_assets/css/layout.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('layout_assets/css/components.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('layout_assets/css/colors.min.css') }}" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script src="{{ asset('global_assets/js/main/jquery.min.js') }}"></script>
	<script src="{{ asset('global_assets/js/main/bootstrap.bundle.min.js') }}"></script>
	<script src="{{ asset('global_assets/js/plugins/loaders/blockui.min.js') }}"></script>
	<script src="{{ asset('global_assets/js/plugins/ui/slinky.min.js') }}"></script>
	<script src="{{ asset('global_assets/js/plugins/ui/ripple.min.js') }}"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	{{-- <script src="{{ asset('global_assets/js/plugins/visualization/d3/d3.min.js') }}"></script> --}}
	{{-- <script src="{{ asset('global_assets/js/plugins/visualization/d3/d3_tooltip.js') }}"></script> --}}
	{{-- <script src="{{ asset('global_assets/js/plugins/forms/styling/switchery.min.js') }}"></script> --}}
	{{-- <script src="{{ asset('global_assets/js/plugins/ui/moment/moment.min.js') }}"></script> --}}
	{{-- <script src="{{ asset('global_assets/js/plugins/pickers/daterangepicker.js') }}"></script> --}}

    <script src="{{ asset('layout_assets/js/app.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/visualization/echarts/echarts.min.js') }}"></script>
	<script src="{{ asset('global_assets/js/demo_pages/dashboard.js') }}"></script>
	{{-- <script src="{{ asset('global_assets/js/demo_charts/pages/dashboard/light/donuts.js') }}"></script> --}}
	{{-- <script src="{{ asset('global_assets/js/demo_charts/pages/dashboard/light/bars.js') }}"></script> --}}
	{{-- <script src="{{ asset('global_assets/js/demo_charts/pages/dashboard/light/pies.js') }}"></script> --}}
    <!-- /theme JS files -->

	<script src="{{ asset('global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
	<script src="{{ asset('global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/demo_pages/form_select2.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>

	<script src="{{ asset('global_assets/js/plugins/notifications/pnotify.min.js') }}"></script>

	<script src="{{ asset('layout_assets/js/app.js') }}"></script>
	<!-- Theme JS files -->
	<script src="{{ asset('global_assets/js/demo_pages/datatables_basic.js') }}"></script>

	{{-- <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/responsive.min.js') }}"></script> --}}

	{{-- <script src="{{ asset('global_assets/js/demo_pages/datatables_responsive.js') }}"></script> --}}


</head>
	<!-- /theme JS files -->
<body>

    @include('layout.header')
    @include('layout.sidebar')

	<!-- Page header -->
    @yield('breadcrumb')
	<!-- /page header -->
	<!-- Page content -->
	<div class="page-content pt-0">

		<!-- Main content -->
		<div class="content-wrapper">

			<!-- Content area -->
			<div class="content">
                @yield('content')

			</div>
			<!-- /content area -->

		</div>
		<!-- /main content -->

	</div>
	<!-- /page content -->

    @include('layout.footer')

    @isset($succes)
        <script>
            $(document).ready(function() {
                (  new PNotify({
                    title: 'Berhasil',
                    text: '{{ $succes }}',
                    addclass: 'bg-primary border-primary',
                    type: 'info',
                    addclass: 'bg-success border-danger',
                    hide: false
                }));
            });
        </script>
    @endisset
    @isset($gagal)
        <script>
            $(document).ready(function() {
                (  new PNotify({
                    title: 'Terjadi kesalahan',
                    text: '{{ $gagal }}',
                    addclass: 'bg-primary border-primary',
                    type: 'info',
                    addclass: 'bg-danger border-danger',
                    hide: false
                }));
            });
        </script>
    @endisset
    @if ($message = Session::get('gagal'))
        <script>
            $(document).ready(function() {
                (  new PNotify({
                    title: 'Terjadi kesalahan',
                    text: '{{ $message }}',
                    addclass: 'bg-primary border-primary',
                    type: 'info',
                    addclass: 'bg-danger border-danger',
                    hide: false
                }));
            });
        </script>
    @endif




</body>
</html>
