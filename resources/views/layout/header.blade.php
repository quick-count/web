
	<!-- Main navbar -->
	<div class="navbar navbar-expand-md navbar-dark bg-indigo">
		<div class="navbar-brand wmin-0 mr-5">
            <a href="{{ route('dashboard') }}" class="d-inline-block">
				{{-- <img src="../../../../global_assets/images/logo_light.png" alt=""> --}}
			</a>
		</div>

		<div class="d-md-none">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
				<i class="icon-tree5"></i>
			</button>
		</div>

		<div class="collapse navbar-collapse" id="navbar-mobile">

			<ul class="navbar-nav ml-auto">
				<li class="nav-item dropdown dropdown-user">
					<a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
						<img src="../../../../global_assets/images/placeholders/placeholder.jpg" class="rounded-circle mr-2" height="34" alt="">
						<span>{{ Session::get('user') }}</span>
					</a>

					<div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="{{ route('act.logout') }}" onclick="event.preventDefault();  document.getElementById('logout-form').submit();"><i
                            class="icon-switch2"></i>
                            {{ __('Logout') }}
                        </a>
                    </div>


                    <form id="logout-form" action="{{ route('act.logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
				</li>
			</ul>
		</div>
	</div>
	<!-- /main navbar -->
