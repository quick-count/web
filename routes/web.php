<?php

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\UserController;
use App\Http\Middleware\Admin;
use App\Http\Middleware\guest;
use App\Http\Middleware\isLogin;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [LoginController::class, 'dashboard'])->name('general.login');
Route::middleware([guest::class])->group(function () {
    Route::get('login', [LoginController::class, 'index'])->name('login');
    Route::post('login', [LoginController::class, 'actLogin'])->name('act.login');
});

Route::middleware([isLogin::class])->group(function () {
    Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');
    Route::post('logout', [DashboardController::class, 'logout'])->name('act.logout');

});


Route::middleware([Admin::class])->group(function () {
    // Route::get('/', [DashboardController::class, 'index'])->name('dashboard');
    Route::get('rekap', [DashboardController::class, 'rekapDaerah'])->name('rekap');
    Route::post('rekap', [DashboardController::class, 'rekapDaerah'])->name('act.rekap');
    Route::get('rekap/form', [DashboardController::class, 'rekapformc1'])->name('rekap.form');
    Route::post('rekap/form', [DashboardController::class, 'rekapformc1'])->name('act.rekap.form');
    Route::get('user', [UserController::class, 'index'])->name('user.view');
    Route::post('user', [UserController::class, 'index'])->name('act.user.cari');
    Route::post('user/detail', [UserController::class, 'detail'])->name('act.user.detail');
    Route::post('user/formc1', [UserController::class, 'formc1'])->name('act.user.form');
    // Route::get('user', [UserController::class, 'index'])->name('act.create.user');
    // Route::get('user', [UserController::class, 'index'])->name('act.update.user');

});


Route::get('get/kelurahan', [DashboardController::class, 'actGetKelurahan'])->name('get.kelurahan');


