<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class BaseController extends Controller
{

    protected $baseURL  = "http://quickgo:8080/";
    public $header = array();
    protected $request;
    public function __construct(Request $request)
    {

        $this->request = $request;
        // $this->middleware('admin');
        // $this->middleware(function ($request, $next) {

        //     return $next($request);
        // });
        // $this->getToken($request);
    }
    public function getToken(){
        if ($this->request->session()->exists('jwt') ) {
            $this->header = array("Authorization"=> "Bearer ".$this->request->session()->get("jwt"));
            return 200;
        }
        return 500;
    }
    public function login(){
    }
    public function delete(){
    }
    public function put(){

    }
    public function post($url,$data){
        try {
            if($this->getToken() != 200){
                return array(
                    "status"=>500,
                    "massage"=>"gagal",
                    "data"=>array()
                );
            }
            $response = Http::withHeaders($this->header)->post($this->baseURL.$url, $data);
            $data = json_decode($response->getBody()->getContents(),true);
            if ($response->status() != 200){
                return array(
                    "status"=>$response->status(),
                    "massage"=>$data['Status'],
                    "data"=>array()
                );
            }
            return array(
                "status"=>200,
                "massage"=>"Succes",
                "data"=>$data['Data']
            );
        } catch (\Exception $th) {
            return array(
                "status"=>500,
                "massage"=>$th->getMessage(),
                "data"=>array()
            );
        }



    }
    public function get($url,$data){
        try {
            if($this->getToken() != 200){
                return array(
                    "status"=>500,
                    "massage"=>"gagal",
                    "data"=>array()
                );
            }
            $response = Http::withHeaders($this->header)->get($this->baseURL.$url, $data);
            $data = json_decode($response->getBody()->getContents(),true);
            if ($response->status() != 200){
                return array(
                    "status"=>$response->status(),
                    "massage"=>$data['Status'],
                    "data"=>array()
                );
            }
            return array(
                "status"=>200,
                "massage"=>"Succes",
                "data"=>$data['Data']
            );
        } catch (\Exception $th) {
            return array(
                "status"=>500,
                "massage"=>$th->getMessage(),
                "data"=>array()
            );
        }



    }


}
