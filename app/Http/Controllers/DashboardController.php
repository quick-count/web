<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends BaseController
{


    protected $parent;
    public function __construct(Request $request)
    {
        parent::__construct($request);
        // $getToken = parent::getToken($request);
        // if( $getToken != 'succes'){
        //     $this->logout($request);
        // }
    }
    public function index(){
        $content = array(
           "data"=> array(
                "hasil_suara_satu"=> 0,
                "hasil_suara_kosong"=> 0,
                "total_suara"=> 0,
                "suara_tidak_sah"=> 0
           )
        );

        $data = $this->get('rekap',array());
        if($data['status']==200){
            $content['data'] = $data['data']['Rekap'];
            $content['total'] = $data['data']['TotalForm'];
        }

        return view('pages.home')->with($content);
    }


    public function rekapDaerah(Request $request){
        $content = array();
        $data = $this->get('kecamatan',array());
        if($data['status']!=200){
            $content['gagal'] = "Gagal ambil data";
        }
        $content['data']['kecamatan'] = $data['data'];
        $content['title'] = "";
        if($request->method('POST')){
          if ($request->has('kecamatan')) {
                $url = "rekap/";
                if ($request->has('kelurahan')) {
                    $url .= "kelurahan/".$request->get('kelurahan');
                }else{
                    $url .= "kecamatan/".$request->get('kecamatan');
                }
                $data = $this->get($url,array());
                if($data['status']==200){
                    $content['data']['rekap'] = $data['data'];
                    $getName = collect($data['data']['Rekap']);
                    $content['data']['name'] =  json_encode($getName->pluck('Name'));
                    $content['data']['hasil_suara_kosong'] =  json_encode($getName->pluck('hasil_suara_kosong'));
                    $content['data']['hasil_suara_satu'] =  json_encode($getName->pluck('hasil_suara_satu'));
                    $content['title'] = $data['data']['Title'];
                }else{
                    $content['gagal'] = $data['massage'];
                }
          }
        }
        return view('pages.client.rekapDaerah')->with($content);
    }

    public function rekapformc1(Request $request){
        $content = array();
        $data = $this->get('kecamatan',array());
        if($data['status']!=200){
            $content['gagal'] = "Gagal ambil data";
        }
        $content['data']['kecamatan'] = $data['data'];
        $content['title'] = "";
        if($request->method('POST')){
          if ($request->has('kecamatan')) {
                $url = "rekap/formc1/";
                if ($request->has('kelurahan')) {
                    $url .= "kelurahan/".$request->get('kelurahan');
                }else{
                    $url .= "kecamatan/".$request->get('kecamatan');
                }
                $data = $this->get($url,array());
                if($data['status']==200){
                    $content['data']['formc1'] = $data['data'];
                    // return $content;
                    // return $data['data'];
                    // $content['data']['rekap'] = $data['data'];
                    // $getName = collect($data['data']['Rekap']);
                    // $content['data']['name'] =  json_encode($getName->pluck('Name'));
                    // $content['data']['hasil_suara_kosong'] =  json_encode($getName->pluck('hasil_suara_kosong'));
                    // $content['data']['hasil_suara_satu'] =  json_encode($getName->pluck('hasil_suara_satu'));
                    // $content['title'] = $data['data']['Title'];
                }else{
                    $content['gagal'] = $data['massage'];
                }
          }
        }
        // echo "<pre>";
        // print_r($content);
        // echo "</pre>";

        // return;
        return view('pages.client.rekapc1')->with($content);
    }





    public function actGetKelurahan(Request $request){
        $id = $request->get('id');
        $url = 'kelurahan/'.$id;
        $data = $this->get($url,array());
        return response($data);
    }

    public function actRekapformc1(){
    }

    public function actRekapDaerah(){

    }

    public function logout(Request $request){
        $request->session()->flush();
        return redirect('login');
    }
}
