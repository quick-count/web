<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends BaseController
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
        // $getToken = parent::getToken($request);
        // if( $getToken != 'succes'){
        //     $this->logout($request);
        // }
    }

    public function formc1(Request $request){
        $content = array(
            "user_id"=>$request->get('userid'),
            "tps_id"=>$request->get('tpsid'),
            "hasil_suara_satu"=> (int)  $request->get('suarasatu'),
            "hasil_suara_kosong"=> (int)  $request->get('suarakosong'),
            "total_suara"=> (int) $request->get('totalsuara'),
            "suara_tidak_sah"=> (int) $request->get('tidaksah')
        );
        $get = $this->post('admin/formc1',$content);
        if ($get['status']!=200) {
                return redirect('user')->with('gagal','Gagal tambah form c1');
        }
        $content['data'] = $get['data'];
        return redirect('user')->with('succes','Berhasil tambah form c1');
    }


    public function detail(Request $request){
        $content = array(
            "data"=> array(),
            "c1" => array(
                "hasil_suara_satu"=> 0,
                "hasil_suara_kosong"=> 0,
                "total_suara"=> 0,
                "suara_tidak_sah"=> 0
            )
        );
        if($request->has('id')){
            $get = $this->get('get/user/'.$request->get('id'),array());
            if($get['status'] != 200){
                return view('pages.users.detail')->with('gagal','Tidak dapat melihat user');
            }
            $content['data'] = $get['data'];
            $getC1 = $this->get('admin/formc1/'.$request->get('id'),array());
            if($get['status'] == 200){
               if (count($getC1['data']) > 0) {
                    $content['c1'] = $getC1['data'];
               }
            }


        }
        return view('pages.users.detail')->with($content);
    }
    public function index(Request $request){

        $content = array();
        if($request->method('POST')){
          if ($request->has('jenis')) {

                $payload = array(
                    "Jenis"=>$request->get('jenis'),
                    "Text"=>$request->get('cari')
                );

                if ($request->get('cari') == "" ){
                    $content['gagal'] = "Text harus diisi";
                    return view('pages.users.index')->with($content);

                }
                $data = $this->post('users/getall',$payload);
                if($data['status']==200){
                    $content['data']['users'] = $data['data'];
                }else{
                    $content['gagal'] = $data['massage'];
                }


          }
        }
        return view('pages.users.index')->with($content);
    }




}
