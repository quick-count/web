<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class LoginController extends Controller
{
    protected $baseURL  = "http://quickgo:8080/";
    protected $jwtToken;

    public function __construct(Request $request) {
        $this->middleware(function ($request, $next) {
            if ($request->session()->exists('jwt') ) {
                return redirect('dashboard');
            }
            return $next($request);
        });

    }


    public function dashboard(){
        $content = array(
            "data"=> array(
                 "hasil_suara_satu"=> 0,
                 "hasil_suara_kosong"=> 0,
                 "total_suara"=> 0,
                 "suara_tidak_sah"=> 0
            )
         );

         $data = $this->get('rekap/tamu',array());
         if($data['status']==200){
             $content['data'] = $data['data']['Rekap'];
             $content['total'] = $data['data']['TotalForm'];
         }

        return view('welcome')->with($content);
    }
    public function index(){
        return view('login');
    }

    public function actLogin(Request $request) {
            $credentials = $this->validate($request, [
                'username'  => 'required|min:3',
                'password' => 'required|min:5',
            ]);
        try {
            $data = array(
                "username"=>$credentials['username'],
                "password"=>$credentials['password']
            );
            $response = Http::post($this->baseURL."auth", $data);

            if ($response->status() != 200){
                return redirect()->back()->with(['gagal' => 'Username atau password salah']);
            }
            $data = json_decode($response->getBody()->getContents(),true);
            $tokenParts = explode(".", $data["Data"]["credential"]["access_token"]);
            if(count($tokenParts) < 3){
                return redirect()->back()->with(['gagal' => 'Username atau password salah']);
            }
            $tokenPayload = base64_decode($tokenParts[1]);
            $jwtPayload = json_decode($tokenPayload,true);
            $request->session()->put([
                'user'=>$jwtPayload['username'],
                'role'=>$jwtPayload['role'],
                'userid'=>$jwtPayload['user'],
                'jwt'=>$data["Data"]["credential"]["access_token"],
                ]);
            return redirect('/');
        } catch (\Exception $th) {
            return redirect()->back()->with(['gagal' => $th->getMessage()]);
        }

    }

    public function get($url,$data){
        try {
            $response = Http::get($this->baseURL.$url, $data);
            $data = json_decode($response->getBody()->getContents(),true);
            if ($response->status() != 200){
                return array(
                    "status"=>$response->status(),
                    "massage"=>$data['Status'],
                    "data"=>array()
                );
            }
            return array(
                "status"=>200,
                "massage"=>"Succes",
                "data"=>$data['Data']
            );
        } catch (\Exception $th) {
            return array(
                "status"=>500,
                "massage"=>$th->getMessage(),
                "data"=>array()
            );
        }



    }
}
